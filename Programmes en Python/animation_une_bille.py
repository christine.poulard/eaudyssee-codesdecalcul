# Christine Poulard, 2 décembre 2022, INRAE
#
import random
from matplotlib import pyplot as plt

import SacDeBilles as SdB
from matplotlib.lines import Line2D
import glob
from PIL import Image

#remplacez par votre chemin
chemin = "C:/WorkSpace/2023-HydroDemo/Illustrations/"

def make_gif(frames):

    frame_one = frames[0]
    frame_one.save(chemin+"animation.gif", format="GIF", append_images=frames,
               save_all=True, duration=400, loop=0)


def tirage_de_billes_en_bloc_pour_anim(i):

    global nb_billes_tirees
    n_billes = nlig * ncol
    liste_x = []
    liste_y = []
    liste_x_tot = []
    liste_y_tot = []
    # liste_freq = []
    liste_couleurs_tot = []
    n_billes = nlig * ncol

    for x in range(ncol):
        for y in range(nlig):
            liste_x_tot.append(x)
            liste_y_tot.append(y)

            freq = SdB.tirage_une_bille(partition_freq.nombre_billes_du_sac) / partition_freq.nombre_billes_du_sac
            coul, cle = partition_freq.attribuer_couleur(freq)
            liste_couleurs_tot.append(coul)

            # print(x,y,freq, cle, coul)
            partition_freq.dico_resultat_un_tirage[cle] += 1

    dico_axes['visu'].clear()
    dico_axes['visu'].axis('off')

    fic_destination = chemin + nom_fic + f"_{i}_b.png"
    plt.savefig(fic_destination)
    frames.append(Image.open(fic_destination))

    taille = (100 / n_billes) * 100

    points_autres = dico_axes['visu'].scatter(liste_x_tot, liste_y_tot, c=liste_couleurs_tot, s=taille)

    prem = True
    dico_axes['bpartir'].clear()
    dico_axes['bpartir'].axis('off')



    print(" frame #", i)
    nb_billes_tirees += n_billes
    dico_axes['bpartir'].set_title(f"Rappel des {nb_billes_tirees} billes tirées")

    for cle, intervalle in partition_freq.dico_freq.items():
        print(f"cle : {intervalle.description}, {partition_freq.dico_resultat_un_tirage[cle]} bille(s) sortie(s)")

    handles = [
        Line2D([0], [0], marker='o', color="b", label=f"{partition_freq.dico_resultat_un_tirage[cle]}"
                      f" ({partition_freq.dico_resultat_un_tirage[cle]/nb_billes_tirees:.2f})",
               markeredgecolor='black',  ls='None',
               markerfacecolor=intervalle.couleur, markersize=15) for cle, intervalle in partition_freq.dico_freq.items()]
    dico_axes['bpartir'].legend(handles=handles, ncol=4, fontsize=12, title_fontsize=12, title=" ",
                  labelspacing=1.5)

    fic_destination = chemin+nom_fic+f"_{i}.png"
    plt.savefig(fic_destination)
    frames.append(Image.open(fic_destination))



if __name__ == '__main__':
    # PREPARATION

    n_billes_max_avec_affichage_courantes = 1000
    nb_billes_tirees = 0

    nlig, ncol = 1, 1
    mosaique = [ ['visu', 'légende'],
                ['bpartir', 'bpartir']]

    fig = plt.figure(tight_layout=True)
    fig.canvas.manager.set_window_title('SacDeCrues - animation')
    dico_axes = fig.subplot_mosaic(mosaique, height_ratios=[1, 1], width_ratios=[1, 1])

    # ax_visu, ax_legende, ax_bilan, ax_btirages,
    partition_freq = SdB.PartitionFrequence( dico_axes['visu'], dico_axes['légende'], None, None)
    partition_freq.construire_legende()


    if ncol * nlig == 1:
        dico_axes['visu'].set_title(f"Tirage aléatoire d'une bille à la fois", fontsize=11)
    else:
        dico_axes['visu'].set_title(f"Tirage aléatoire par série de {ncol * nlig} billes", fontsize=11)
    dico_axes['visu'].axis('off')
    dico_axes['légende'].axis('off')
    dico_axes['bpartir'].clear()
    dico_axes['bpartir'].axis('off')
    dico_axes['bpartir'].set_title(f"Nombre de billes rares par série de {ncol * nlig}", fontsize=11)



    nom_fic = f'test_100_tirages_{nlig * ncol}billes'
    print("L'animation sera enregistrée ici : ", nom_fic)


    # tirages
    frames = []
    for i in range(100):
        tirage_de_billes_en_bloc_pour_anim(i)

    make_gif(frames)
