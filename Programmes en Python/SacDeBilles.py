# Simulateur de tirage dans un sac de billes
#
# Christine Poulard, Novembre 2022
#
#

"""
Codage initial
rouge : 1 sur 100 , soit autant de chances que la crue la plus forte ait un débit au moins centennal en un point donné d'une rivière l'année prochaine, ou n'importe quelle année donnée
bleu : 1 sur 100 = débit moins cinquantennal mais au plus centennal...
vert : 8 sur 100 = débits supérieur au débit décennal mais inférieur au cinquantennal.
noir : 90 sur 100 = pas de débit supérieur au débit décennal une année donnée.
"""

from matplotlib import pyplot as plt
from matplotlib.widgets import TextBox, Button
# from matplotlib.colors import ListedColormap, BoundaryNorm
from matplotlib.lines import Line2D
import matplotlib.ticker as mticker
from tkinter.simpledialog import askinteger
#import matplotlib
import random
from typing import NamedTuple

# https://matplotlib.org/stable/gallery/user_interfaces/toolmanager_sgskip.html
from matplotlib.backend_tools import ToolBase, ToolToggleBase

#print ('matplotlib: {}'.format (matplotlib.__version__))
# random.randint(start, stop)  (both included)
# numpy.random.randint # random.randint(low, high=None, size=None, dtype=int) : plus performant mais nécessite numpy

plt.rcParams['toolbar'] = 'toolmanager'

dico_axes = dict()
partition_freq = None

class Tirage(ToolBase):
    """ Nouveau tirage ; on vérifie si le type de tirage a changé"""
    default_keymap = 'G'  # keyboard shortcut
    description = 'Nouveau tirage'

    def trigger(self, *args, **kwargs):
        global n_tirages_meme_ligne_col

        if ncol != ncol_prec or nlig != nlig_prec:
            maj_titre(fig)
        if ncol * nlig != ncol_prec * nlig_prec:
            n_tirages_meme_ligne_col = 0
            for cle in partition_freq.dict_set:
                partition_freq.dict_set[cle] = []

        # tirage_de_billes_en_bloc(dico_axes['visu'], nlig=nlig, ncol=ncol)
        tirage_de_billes_en_bloc_actuel(partition_freq) #, nlig, ncol)
        plt.draw()

class RAZ(ToolBase):
    """ Remise à zéro de tous les compteurs """
    default_keymap = 'Z'  # keyboard shortcut
    description = 'RAZ des compteurs'

    def trigger(self, *args, **kwargs):
        global n_tirages, n_billes_tirees, n_tirages_meme_ligne_col

        n_tirages, n_billes_tirees, n_tirages_meme_ligne_col = 0, 0, 0
        partition_freq.raz()


class New_ncol(ToolBase):
    """ Changement du nombre des colonnes """
    default_keymap = 'c'  # keyboard shortcut
    description = 'Modification du numéro de colonnes'

    def trigger(self, *args, **kwargs):
        global ncol, dict_set
        entry_col = askinteger("Changement du nombre de colonnes", "nouveau nombre de colonnes")
        if entry_col != 0:
            ncol = abs(entry_col)
            text_ncolonnes.set_val(f'{ncol}')
        # maj_titre(fig) : le titre doit correspondre au dernier tirage affiché

class New_nlig(ToolBase):
    """ Changement du nombre des colonnes """
    default_keymap = 'l'  # keyboard shortcut
    description = description = 'Modification du numéro de lignes'

    def trigger(self, *args, **kwargs):
        global nlig, dict_set
        entry_lig = askinteger("Changement du nombre de lignes", "nouveau nombre de lignes")
        if entry_lig != 0:
            nlig = abs(entry_lig)
            text_nlignes.set_val(f'{nlig}')
        # maj_titre(fig) : le titre doit correspondre au dernier tirage affiché

# OUTILS
# Helper function used for visualization in the following examples
def identify_axes(ax_dict, fontsize=12):
    """
    Helper to identify the Axes in the examples below.

    Draws the label in a large font in the center of the Axes.

    Parameters
    ----------
    ax_dict : dict[str, Axes]
        Mapping between the title / label and the Axes.
    fontsize : int, optional
        How big the label should be.
    """
    kw = dict(ha="center", va="center", fontsize=fontsize, color="darkgrey")
    for k, ax in ax_dict.items():
        ax.text(0.5, 0.5, k, transform=ax.transAxes, **kw)

# CLASSES
class Intervalle(NamedTuple):
    freq_debut: float
    freq_fin: float
    couleur: str
    description: str

class PartitionFrequence:
    nombre_billes_du_sac = 100
    epsilon = 0.001
    def __init__(self, ax_visu, ax_legende, ax_bilan, ax_btirages, nb_billes_sac=None ):
        self.ax_visu= ax_visu
        self.ax_legende = ax_legende
        self.ax_bilan = ax_bilan
        self.ax_btirages = ax_btirages
        self.dico_freq = dict()
        self.dico_resultat_un_tirage = dict()
        self.dico_resultat_tous_tirages = dict()
        self.couleur_max = None

        self.liste_couleurs_ordonnees = []
        self.liste_effectifs_ordonnes = []

        self.composition_du_sac()
        self.calcul_couleurs_legende()
        self.verif_composition()

        if nb_billes_sac is not None :
            self.nombre_billes_du_sac = nb_billes_sac

    def composition_du_sac(self):
        self.cle_anomalie = 'trou dans la raquette !'
        self.couleur_anomalie = 'yellow'
        self.dico_freq['noire(s)'] = Intervalle(0, 0.9, 'black', " T =< 10 ans") # "'...inférieure à la décennale')
        self.dico_freq['verte(s)'] = Intervalle(0.9, 0.98, 'green', "10 ans < T =< 50 ans") #'...entre la décennale \net la cinquantennale')
        self.dico_freq['bleue(s)'] = Intervalle(0.98, 0.99, 'blue',  "50 ans < T =< 100 ans") #'... entre la cinquantennale \net la centennale')
        self.dico_freq['rouge(s)'] = Intervalle(0.99, 1, 'red', "T >= 100 ans") #'... supérieure à la centennale')
        self.dict_set = dict()
        self.raz()

    def verif_composition(self):
        print("VERIFICATION DU SAC")
        dico_nb_billes = dict()
        verif_proba = 0

        for cle, intervalle in self.dico_freq.items() :
            intervalle = abs (intervalle.freq_fin - intervalle.freq_debut)
            verif_proba += intervalle

        plt.ion()
        fig_sac, ax_sac = plt.subplots()
        fig_sac.canvas.manager.set_window_title('Composition du Sac de Crues')
        ax_sac.axis("off")
        titre = f"C O M P O S I T I O N   D U  S A C \n de {self.nombre_billes_du_sac} billes"
        handles = [Line2D([0], [0], marker='o', color="b", ls='None', label=f"{label}  ({label/self.nombre_billes_du_sac:.3f})", markeredgecolor='black',
                              markerfacecolor=couleur, markersize=15) for label, couleur in
                       zip(self.liste_effectifs_ordonnes, self.liste_couleurs_ordonnees)]
        ax_sac.legend(handles=handles, ncol=1, fontsize=8, title_fontsize=10, title=titre,
                                   labelspacing=1.5)
        ax_sac.set_title(f"Vérification de l'effectif total {sum(self.liste_effectifs_ordonnes) } \n somme des probas = {verif_proba :.3f}")
        plt.ioff()

    def raz(self):
        for cle in self.dico_freq:
            self.dico_resultat_un_tirage[cle] = 0
            self.dico_resultat_tous_tirages[cle] = 0
            self.dict_set[cle] = []

    def attribuer_couleur(self, freq):
        # la fréquence 0 n'est jamais atteinte mais 1 oui (les valeurs vont de 1 au max)
        code_couleur, cle = None, None
        for cle, intervalle in self.dico_freq.items():
            petite_freq = min (intervalle.freq_debut, intervalle.freq_fin)
            grande_freq = max(intervalle.freq_debut, intervalle.freq_fin)
            if petite_freq < freq <= grande_freq:
                code_couleur = intervalle.couleur
                break
        if code_couleur is None:
            code_couleur = self.couleur_anomalie
        return code_couleur, cle

    def calcul_couleurs_legende(self):
        set_freq = {0, 1}
        for intervalle in self.dico_freq.values():
            set_freq.add (intervalle.freq_debut)
            set_freq.add(intervalle.freq_fin)
        bounds = sorted(list(set_freq))

        liste_freq_moy = []

        liste_labels = []
        self.liste_cles = []
        for deb, fin in zip(bounds[:-1], bounds[1:]):
            freq_moy = (deb + fin) / 2
            liste_freq_moy.append(freq_moy)
            # print(deb, fin, abs(deb - fin), self.nombre_billes_du_sac, abs(deb-fin) * self.nombre_billes_du_sac)
            self.liste_effectifs_ordonnes.append(round(abs(deb-fin) * self.nombre_billes_du_sac))
            code_couleur, cle = self.attribuer_couleur(freq_moy)
            self.liste_couleurs_ordonnees.append(code_couleur)
            liste_labels.append(self.dico_freq[cle].description)
            self.liste_cles.append(cle)

        self.code_premiere_couleur = self.liste_couleurs_ordonnees[0]

        # attention,gros piège, les couleurs d'une cmap sont "gauche incluse, droite exclue" et ça ne nous convient pas
        # self.cmap = ListedColormap(self.liste_couleurs_ordonnees)
        # self.norm = BoundaryNorm(bounds, cmap.N)

        self.liste_labels = liste_labels

    def construire_legende(self):
        # dessin de la légende
        titre = "C O D E   C O U L E U R \n \n de la période de retour T \n de la valeur max de l'année"
        handles = [Line2D([0], [0], marker='o', color="b", ls = 'None', label=label, markeredgecolor= 'black',
                          markerfacecolor=couleur, markersize=15) for label, couleur in zip(self.liste_labels, self.liste_couleurs_ordonnees)]
        self.ax_legende.legend(handles=handles, ncol=1, fontsize=8, title_fontsize=10, title=titre,
                                         labelspacing=1.5)


    def legende_bilan(self):

        liste_labels = [f"{self.dico_resultat_un_tirage[cle]} ({self.dico_resultat_tous_tirages[cle]} sur {n_billes_tirees})" for cle in self.liste_cles]

        # dessin de la légende
        if n_tirages ==1 :
            titre = f"B I L A N   D E   L A   S E R I E \n \n "
        else:
            titre = f"B I L A N   D E   L A   S E R I E  #{n_tirages} \n (et cumul sur les {n_billes_tirees} billes)"
        handles = [Line2D([0], [0], marker='o', color="b", ls = 'None', label=label, markeredgecolor= 'black',
                          markerfacecolor=couleur, markersize=15) for label, couleur in zip(liste_labels, self.liste_couleurs_ordonnees)]
        self.ax_bilan.clear()
        self.ax_bilan.axis('off')
        self.ax_bilan.legend(handles=handles, ncol=1, fontsize=8, title_fontsize=10, title=titre,
                                         labelspacing=1.5)

    def raz_resultats_un_tirage(self):
        for cle in self.dico_resultat_un_tirage:
            partition_freq.dico_resultat_un_tirage[cle] = 0

    def maj_resultats_tous_tirages(self):
        for cle in self.dico_resultat_tous_tirages:
            partition_freq.dico_resultat_tous_tirages[cle] += partition_freq.dico_resultat_un_tirage[cle]



# FONCTIONS



def tirage_une_bille(n_billes_sac):

    rang = random.randint(1, n_billes_sac)

    return rang


def tirage_de_billes_en_bloc_actuel(partition_freq, ax:plt.Axes=None): #  nlig=nlig, ncol=ncol):
    """
    seule fonction mise à jour
    """
    global n_tirages, n_billes_tirees, n_tirages_meme_ligne_col, ncol_prec, nlig_prec

    if ax is None:
        ax = partition_freq.ax_visu
    n_max = nlig * ncol
    liste_x = []
    liste_y = []
    liste_x_tot = []
    liste_y_tot = []
    #liste_freq = []
    liste_couleurs=[]
    n_billes = nlig * ncol
    n_tirages += 1
    n_tirages_meme_ligne_col += 1
    n_billes_tirees += n_billes

    partition_freq.raz_resultats_un_tirage()
    print("TIRAGE #", n_tirages)
    for x in range(ncol):
        for y in range(nlig):
            liste_x_tot.append(x)
            liste_y_tot.append(y)

            freq = tirage_une_bille(partition_freq.nombre_billes_du_sac)/partition_freq.nombre_billes_du_sac
            coul, cle = partition_freq.attribuer_couleur(freq)

            # print(x,y,freq, cle, coul)
            partition_freq.dico_resultat_un_tirage[cle] += 1

            if coul != partition_freq.code_premiere_couleur :
                liste_couleurs.append(coul)
                liste_x.append(x)
                liste_y.append(y)
            #liste_freq.append(freq)

    for cle, intervalle in partition_freq.dico_freq.items():
        print(f" Nombre de {cle} ({intervalle.description}) : {partition_freq.dico_resultat_un_tirage[cle] }")
    ax.clear()
    ax.axis('off')

    taille = (100 / n_billes)* 100


    if n_billes <= n_billes_max_avec_affichage_courantes:
        points_frequents = ax.scatter(liste_x_tot, liste_y_tot, c=partition_freq.code_premiere_couleur, s= taille)
        message = ""
    else:
        ax.set_title(f"( avec seulement les billes rares)")
    points_autres = ax.scatter(liste_x, liste_y, c=liste_couleurs, s=taille)

    # if n_tirages == 1 :
    #     ax.set_title(f"première série" + message) # {n_billes_tirees} billes en tout
    # else:
    #     ax.set_title(f"série #{n_tirages}"  + message)  #  {n_billes_tirees} billes en tout

    partition_freq.maj_resultats_tous_tirages()
    partition_freq.legende_bilan()

    # bilan par tirage
    partition_freq.ax_btirages.clear()
    if n_tirages_meme_ligne_col <= 10:
        pas = 1
    else :
        pas = int(n_tirages_meme_ligne_col / 10)
    dico_axes['bpartir'].xaxis.set_major_locator(mticker.MultipleLocator(pas))
    partition_freq.ax_btirages.set_title(f'valeurs fortes par tirages de {ncol * nlig} billes')
    prem = True
    for cle, intervalle in partition_freq.dico_freq.items() :
        nb_pour_cette_cle = partition_freq.dico_resultat_un_tirage[cle]
        partition_freq.dict_set[cle].append(nb_pour_cette_cle)
        if intervalle.couleur != partition_freq.code_premiere_couleur:
            if prem :
                partition_freq.ax_btirages.bar(range(1, n_tirages_meme_ligne_col + 1), partition_freq.dict_set[cle],
                                               color=intervalle.couleur)
                prem = False
                bottom = partition_freq.dict_set[cle]
            else:
                partition_freq.ax_btirages.bar(range(1, n_tirages_meme_ligne_col + 1), partition_freq.dict_set[cle],
                                               color=intervalle.couleur, bottom=bottom)
                bottom = [ i+j for i,j in zip(bottom, partition_freq.dict_set[cle] )]

    plt.draw()
    # réinitialisation
    ncol_prec, nlig_prec = ncol, nlig

    # pour animer  = alpha = 0 puis https://matplotlib.org/stable/gallery/animation/rain.html#sphx-glr-gallery-animation-rain-py

## fonctions widgets

def maj_titre(fig):
    fig.suptitle(f"Tirage de {nlig} x {ncol} billes dans un sac")

# def changement_nb_lig(texte):
#     global nlig
#     text_nlignes.color = 'red'
#     try:
#         n = int(texte)
#         if n <= 0:
#             if n < 0:
#                 n = abs(n)
#             text_nlignes.color = 'yellow'
#         if n != 0 :
#             nlig = min(n, NMAX)
#     except ValueError:
#         text_nlignes.color = 'red'
#     text_nlignes.set_val(f"{nlig}")
#     #fig.draw_idle()
#
# def changement_nb_col(texte):
#     global ncol
#     text_ncolonnes.color = 'red'
#     try:
#         n = int(texte)
#         if n <= 0:
#             if n < 0:
#                 n = abs(n)
#             text_ncolonnes.color = 'yellow'
#         if n != 0 :
#             ncol = min(n, NMAX)
#     except ValueError:
#         text_ncolonnes.color = 'red'
#     text_ncolonnes.set_val(f"{ncol}")

def changement_nb_col(texte):
    global ncol
    text_ncolonnes.color = 'red'
    try:
        n = int(texte)
        if n <= 0:
            if n < 0:
                n = abs(n)
            text_ncolonnes.color = 'yellow'
        if n != 0 :
            ncol = min(n, NMAX)
    except ValueError:
        text_ncolonnes.color = 'red'
    text_ncolonnes.set_val(f"{ncol}")


#CORPS DU PROGRAMME
if __name__ == '__main__':
    NMAX = 100
    n_billes_max_avec_affichage_courantes = 1000

    nlig, ncol = 10, 10
    nlig_prec, ncol_prec = nlig, ncol
    n_tirages = 0
    n_billes_tirees = 0
    n_tirages_meme_ligne_col = 0

    mosaique = [['tbox_nb_lig', 'tbox_nb_col', 'légende'],
                ['visu', 'visu', 'légende'],
                ['visu', 'visu', 'légende_résultat'],
                ['bpartir', 'bpartir', 'légende_résultat']]

    fig = plt.figure(tight_layout = True)
    fig.canvas.manager.set_window_title('SacDeCrues')

    # toolbar
    fig.canvas.manager.toolmanager.add_tool('R.A.Z. cumuls', RAZ)
    fig.canvas.manager.toolmanager.add_tool('Nouveau tirage !', Tirage)
    fig.canvas.manager.toolmanager.add_tool('Nbre col.', New_ncol)
    fig.canvas.manager.toolmanager.add_tool('Nbre lig.', New_nlig)

    fig.canvas.manager.toolbar.add_tool('R.A.Z. cumuls', 'Changements', 1)

    fig.canvas.manager.toolbar.add_tool('Nbre lig.', 'Changements', 2)
    fig.canvas.manager.toolbar.add_tool('Nbre col.', 'Changements', 3)
    fig.canvas.manager.toolbar.add_tool('Nouveau tirage !', 'Tirages du sac', 1)

    # définition des vignettes
    dico_axes = fig.subplot_mosaic(mosaique, height_ratios=[1,2,2,1], width_ratios=[1,1,1])
    partition_freq = PartitionFrequence(dico_axes['visu'], dico_axes['légende'], dico_axes['légende_résultat'], dico_axes['bpartir'])

    partition_freq.construire_legende()
    for ax in dico_axes.values():
        ax.axis('off')

    #print(dico_axes)
    plt.subplots_adjust(left=0.2, bottom=None, right=None, top=None, wspace=None, hspace=0.1)

    # pour débugage
    #identify_axes(dico_axes)

    text_nlignes = TextBox(dico_axes['tbox_nb_lig'], "Nb lignes", initial=nlig, hovercolor='yellow',
                           textalignment='right')
    #dico_axes['tbox_nb_lig'].set_title("Nombre de lignes", fontsize=10)
    #text_nlignes.set_val(nlig, size=20)

    text_ncolonnes = TextBox(dico_axes['tbox_nb_col'], "Nb colonnes", initial=ncol, hovercolor='1', textalignment='right')
    #dico_axes['tbox_nb_col'].set_title("Nombre de colonnes ", fontsize=10)
    dico_axes['bpartir'].set_title(f"Nombre de billes rares par série de {ncol * nlig}", fontsize=11)

    # inactivation des textbox, affichage seul
    #text_nlignes.on_submit(changement_nb_lig)
    #text_ncolonnes.on_submit(changement_nb_col)
    text_nlignes.set_active(False)
    text_ncolonnes.set_active(False)

    # tirage
    maj_titre(fig)

    #tirage_de_billes_en_bloc(dico_axes['visu'], nlig=nlig, ncol=ncol)
    tirage_de_billes_en_bloc_actuel(partition_freq) #, nlig=nlig, ncol=ncol)

    plt.show()
