## CodesDeCalcul pour un billet de blog EauDyssee
Un billet de blog ["la crue centennale la main dans le sac"](https://www.eaudyssee.org/la-crue-centennale-prise-la-main-dans-le-sac/) a été rédigé avec [l'association EauDyssée](https://www.eaudyssee.org/), qui propose "Des ateliers ludiques et créatifs pour tout public".
En complément, vous trouverez ici davantage d'informations techniques et les petits codes de calcul pour jouer avec les notions de probabilité de crues sur 1 année donnée... et sur N années. 

Une application interactive qui simule des tirages de billes a été codée en Python, mais pour permettre au plus grand nombre de jouer avec nous vous proposons aussi des variantes en javascript (il suffit de glisser-déplacer le fichier dans la barre de navigation de firefox ), et même une version... en Scratch !

Le projet est actuellement développé en parallèle en anglais, sous le nom de bag of floods, et l'application html en anglais est actuellement plus riche que la version française (possibilité de tirer des formes d'événements en plus des classes de période de retour).

> Si vous utilisez ces ressources pour un projet pédagogique, signalez-le, on saura que ce travail de mise en forme a été utile et on vous aidera si possible...  (christine.poulard@inrae.fr)

# La « crue centennale » prise la main dans le sac !


Réflexions et outils pour illustrer cette notion faussement familière.

## Une animation  pour discuter de notions "grand public" mais pas si intuitives que cela...
Les idées d'animation ci-dessous seront à adapter selon les publics : scolaires, journées d'information sur la prévention des inondations, préparation à des réunions de concertation...
Les codes existent en plusieurs versions, et chaque code peut être utilisé de façon simple ou plus poussée : ils sont décrits chacun dans une page dédiée.

### Naissance du sac de billes : la Fête de la Science
La démarche "les crues la main dans le sac" a été initiée lors d'une "Fête de la Science", sur une idée de Céline Berni, et a été progressivement affinée et augmentée. 
Autour  des expériences (tirer des billes d'un sac) puis de petits codes de démonstration, cette animation permet de se poser la question de la définition de "**crue centennale**" (mais pas que...) et des variantes de l'expression "crue centennale", de constater la  **variabilité** (tirages au hasard) en comparant des réalisations à la composition connue du sac, et de se placer dans une optique de **prise de décision** : pour aménager le territoire, on raisonne sur une durée de plusieurs dizaines d'années (ce qui correspond à autant de tirages au hasard)...  à quoi puis-je m'attendre sur cette période ? Dans le cas où je ne connaitrais pas la composition du sac... est-ce que je peux la deviner en effectuant des tirages au hasard ? L'air de rien, on aborde le problème de l'**analyse fréquentielle en hydrologie** : comment estimer le **régime des débits** à partir d'observations... (dit autrement : comment ajuster une loi de probabilité sur un échantillon).

### jouer aux billes c'est bien, mais on peut jouer aux cartes aussi
Il est ensuite intéressant de mettre en pratique ces notions pour lire une carte d'aléa inondation, plus compliquée qu'elle n'y paraît. 

## Peut-on jouer aux billes en cours d'hydrologie ?
Une partie de l'animation peut être utilisée en cours d'hydrologie : 
- d'une part pour signaler que cette notion n'est pas forcément facile à cerner, en prévision de discussions avec des interlocuteurs qui ne sont pas tous familiers des probabilités
- d'autre part comme point de départ à des discussions (voir "questions" ci-dessous...),
- enfin, avec les outils plus techniques (tirages d'un échantillon d'après une loi de Gumbel et estimation des quantiles)

### les questions soulevées (par exemple : limites de l'analogie crues / billes)
- réduction aux "crues max annuels" : est-ce suffisant ? 
- réflexion sur les "crues de projet" : les débits peuvent être liés à une période de retour, mais peut-on affecter une **période de retour à des crues**  ?  Comment sont construits les hydrogrammes supposés représentatifs d'une période de retour utilisés dans les codes de calcul hydrauliques pour estimer l'emprise des zones inondées ?
- est-ce que les périodes de retour sont suffisantes pour comprendre l'aléa inondation en un point et donc interpréter une carte d'aléa inondation probabilisée ? Ne vaut-il pas mieux raisonner sur plusieurs années ? Comment ces cartes sont-elles construites ?

### petits codes pour illustrer des notions de stats
- manipuler les probabilités de crue sur plusieurs années (formule d'analyse combinatoire connue dès le lycée)
- initiation à l'hydrologie fréquentielle, comment estimer une relation débit-période de retour à partir d'un échantillon de crues observées sur N années, pourquoi l'estimation est-elle parfois proche ou parfois loin de la solution... (développé pour un TD Sciences de l'Eau de 1ère année d'école d'ingénieurs).

Pour en savoir plus : voir [le wiki de ce site](https://gitlab.irstea.fr/christine.poulard/eaudyssee-codesdecalcul/-/wikis/home).

## Résumé (le wiki détaille davantage les points techniques)
Pourquoi accorder autant d'importance à la crue centennale ? La "crue centennale" est très présente sur les cartes d'aléa inondation, et c'est sans doute celle qui revient le plus dans les journaux d'information, mais cela n'est pas forcément la crue à partir de laquelle les ennuis commencent...  Tout dépend du lieu. 

Dans une étude hydrologique, on s'attache à caractériser le **régime hydrologique** des hautes eaux, notamment en estimant la relation entre le **débit** (noté Q) et la **période de retour** (notée T). On définira cette notion dans la suite du texte, mais on peut déjà annoncer que si T=100 ans, le débit associé Q(T=100 ans) est le fameux **débit centennal**. En hydrologie, on cherche donc une relation entre Q et T pour toute une gamme de T, pas seulement pout T=100 ans...  
C'est pour cela que nous avons tenu dans cette expérience à inclure plusieurs couleurs de bille, pour rappeler qu'il existe toute une gamme de débits possibles et que "la centennale" ne mérite pas l'attention exclusive !
L'évaluation des Dommages Moyens Annualisés (combien en moyenne coûtent les crues, si on fait un calcul moyen sur un très grand nombre d'années) exige d'ailleurs de connaître les zones inondées pour toute la gamme de crues susceptibles de provoquer des dommages.  

```
déroulement des étapes :
  "période de retour des DEBITS (hydrologie)" ;
        --> "Zones inondées pour chaque période de retour étudiée (hydraulique)" ;
                 --> "Dommages pour chaque zone inondée donc pour chauque période de retour (économie, ...)";
                    --> Calcul final = Dommages Moyens Annualisés
 
```


### Quel rapport entre un sac de billes et la probabilité d'inondation sur un tronçon ??
Prenons un cours d'eau ; ou plus raisonnablement un **tronçon de rivière**. En effet, lors d'un événement de crue, tous les affluents ne subissent pas les mêmes intensités de pluie, et l'inondation n'a pas la même ampleur sur tout le réseau. Sur ce tronçon, certaines années la crue maximale sera modeste, d'autres années on verra le cours d'eau déborder, et plus rarement le territoire subira des inondations, plus ou moins étendues...On peut souvent lire que la **crue centennale** est la crue qui "se produit une année sur 100 en moyenne " ; on va comprendre que cette définition est un peu trompeuse...  

Prenons un sac de 100 billes, dont 90 noires, 8 vertes, 1 bleue et 1 rouge...
Il est donc tentant de dire que "tirer une bille rouge" sur les 100 billes possibles, c'est comme "observer une crue centennale" ?

Au lieu de raisonner sur des "crues" on raisonnera sur des débits car cela nous permettra d'utiliser des outils statistiques... notre sac étant un outil de pour jouer avec des probabilités de manière "empirique" ! Ceux qui sont à l'aise avec les maths pourront poursuivre avec des parties plus techniques. On pourra revenir aux "crues probabilistes" en fin de session. 

On explique le lien entre couleur des billes et **classe** de périodes de retour:

- la bille rouge : une parmi 99 autres, elle a don 1/100 de chances d'être tirée. Ce n'est pas la probabilité de "tomber pile sur le débit centennal" qui vaut 1/100, c'est "l'atteindre ou dépasser" ! La bille rouge représente donc la *classe des débits au moins égaux au débit centennal". Dit autrement, j'ai 99 chances sur 100 d'être **en-dessous du débit centennal", donc le complément c'est bien d'être **au-dessus** du débit centennal ! 

- le reste du sac : il est important de ne pas se focaliser sur le seuil centennal. Pourquoi étudier toujours la crue centennale ? C'est une habitude ? un chiffre "rond" proche de l'espérance de vie humaine ? Selon les endroits, les ennuis commencent avant ou après ce seuil, selon la topographie et les aménagements.
On va donc compléter avec quelques billes bleues et vertes pour nuancer. Voici la composition proposée, que l'on va expliquer en dessous.

<img src="/images/compo_sac.png" width="200">   hop, dans le sac <img src="/images/sac_grand_marron.png" width="150">

**1** <img src="/images/bille_rouge_grand.png" width="30"> : 1 chance sur 100 de tirer une bille rouge, soit autant de chances que la crue la plus forte ait un débit **au moins centennal** en un point donné d'une rivière l'année prochaine, ou n'importe quelle année donnée

**1** <img src="/images/bille_bleue_grand.png" width="30"> : certes, on a 2 chances sur 100 d'observer un débit au moins cinquantennal une année donnée, mais comme il y a déjà une bille rouge représentant les débits au moins centennaux...on place donc une seule bille bleue pour représenter les crues **au moins cinquantennales mais au plus centennales**...

**8** <img src="/images/bille_verte_grand.png" width="30"> : on a 1 chance sur 10 de dépasser le débit décennal une année donnée... Donc il faudrait 10 billes sur 100 ; cependant les billes bleue et rouge représentent déjà des tirages avec des débits supérieurs au débit décennal, il faut donc donc les déduire de l'effectif. Il reste 8 billes vertes représentant la classe de débits **supérieur au débit décennal mais inférieur au cinquantennal**.

**90** <img src="/images/bille_noire_grand.png" width="30">: 90 chances sur 100 de tirer une bille noire, soit 9 chances sur 10, comme la probabilité ne pas observer de débit **inférieur au débit décennal** une année donnée.

### En quoi cela peut-il nous aider à comprendre et manipuler correctement la notion de probabilité de "crue"... ou plutôt de débit ??

Finalement, la bille rouge ne correspond pas à "un débit centennal", mais représente à la classe de toutes valeurs **dépassées** avec une fréquence supérieure à 1/100. Ainsi, on a une chance sur 100 de connaître une année donnée une crue de débit "**supérieur ou égal** au débit centennal".

On peut exprimer le débit de **période de retour N années** de plusieurs façons :
     => c'est le débit **dépassé** en moyenne **une fois toutes les N années**...
     => c'est le débit **dépassé** en moyenne **une année sur N**...
     => ce débit a **une chance sur N** d'être dépassée l'année prochaine (ou n'importe quelle autre année) !  

### Pourquoi est-ce intéressant de tirer plusieurs billes successivement ? Est-ce que je dois remettre les billes dans le sac entre deux tirages ??
Il est déjà intéressant de comprendre que l'on a une chance sur cent de subir une crue centennale...  ah non, pardon, **un débit au moins centennal** chaque année. Mais je ne construis pas une maison ou une infrastructure pour un an : j'ai besoin en fait d'évaluer la probabilité d'être inondé sur une longue période, 10, 15 ou 50 ans...
On peut commencer à réfléchir en tirant des billes du sac : je suis plutôt confiant de ne pas tirer de bille rouge la première fois, il n'y en a qu'une sur cent... mais j'ai bien conscience que plus je prévois de faire de tirages, plus j'ai de "chance" de tomber dessus... 
Attention, la probabilité de "tirer une rouge" n'évolue pas, elle reste de 1/100 car ce sont des tirages "avec remise", c'est-à-dire que je remets chaque bille tirée dans le sac. En effet, la composition du sac doit rester inchangée, car l'aléa inondation (=le phénomène physique) n'est pas affecté par l'historique. Si je subis une forte crue une année donnée, je n'ai pas de garantie d'être tranquille pour quelques temps... 

Comme c'est un peu fastidieux de tirer physiquement des billes d'un sac, et qu'un tirage n'est qu'une réalisation parmi tout l'éventail des possibles, on va coder un petit programme pour tirer à notre place cent billes autant de fois que l'on voudra : avec le code en Scratch ou son équivalent en Python, qui offre davantage de fonctionnalités.
L'un ou l'autre illustrent qu'avec le même sac, de composition connue, j'ai parfois peu de crues fortes, d'autres fois j'en ai beaucoup... c'est le hasard ! 
<img src="/images/NeufTiragesDeCentCrues.png" width="300">

Avec un code, on peut facilement effectuer des tirages successifs de séries de 100 billes. En les cumulant, les fréquences empiriques de chaque bille doivent rejoindre les fréquences théorique ; le code SacDeBilles.py facilite cette vérification avec des bilans  [Voir résultat de 10x100 tirages, et même de 100x100](../-/wikis/Sac%20de%20billes%20version%20Python).
En même temps, on constate une grande **variabilité** entre les séries! Chaque tirage de 100 billes a sa "personnalité"...  0 bille rouge ou une, ou deux, ou trois, très rarement 4... mais ça arrive. 
A force de tester le code, on tombe sur des tirages atypiques : <img src="/images/DeuxTiragesAtypiques.PNG" width="400">

Cela doit nous faire réfléchir sur l'interprétation des observations : une plaine "sans histoire" peut avoir une probabilité d'inondation faible... ou bien elle a eu de la chance... jusqu'à présent.

### Peut-on évaluer la variabilité des réalisations ?
On peut évaluer **empiriquement** la variabilité des tirages, par exemple relever pour chaque groupe de 100 tirages si on obtient 0, 1 ou plus de deux crues centennales, pour estimer la probabilité de chacun de ces cas, mais il faudra multiplier les simulations pour que les chiffres se stabilisent. 
On peut aussi déterminer ces probabilités **mathématiquement**.

L'un des codes en Python permet de [calculer la probabilité d'avoir k crues de période de retour au moins égale à T en N années](https://gitlab.irstea.fr/christine.poulard/eaudyssee-codesdecalcul/-/blob/master/Programmes%20en%20Python/ProbaCruesMaxAn_SurNannees_stairs.py) 
Le texte plus détaillé présentera la formule (pour les lycéens et au-delà) ; on peut être surpris de calculer que l'on a environ une chance sur trois de ne pas avoir une crue centennale en cent ans, une chance sur trois d'en subir exactement une, et une chance sur trois d'en voir passer... deux et plus !

<img src="/images/ProbaCruesCentennalesEn100ans.png"  width="300">

Avec ce code, on peut par exemple répondre à la question "si mon bien est inondé en moyenne tous les 50 ans, quelle est la probabilité que je sois inondé une fois dans les 20 prochaines années ?  ou deux fois ?"... ce qui finalement est une meilleure formulation que de considérer la probabilité d'être inondé pour chaque année séparément.


### je découvre sur une carte que j'habite en "zone de crue centennale" : est-ce que j'ai une "chance" sur cent d'être inondé chaque année ???
Les services de l'Etat publient différentes cartes représentant l'aléa inondation (PPRI, Plans de Prévention des Risques Inondation : cartes établies pour la Directive Inondation dans les Territoires à Risque d'Inondation...), qui sont publiques mais parfois un peu délicates à interpéter car il faut aller chercher des informations qui ne sont pas sur la carte mais dans un rapport séparé : quelle est la période de retour de la zone inondée,, ou les périodes de retour des zones inondées ? Est-ce que tous types de phénomènes pouvant provoquer une inondation sont pris en compte, ou seulement  le débordement du cours d'eau principal ? Les instrastructures existantes (digues...) sont-elles prises en compte dans la modélisation ?

Prenons un exemple dans un Terrioitre à Risque d'Inondation, où le rapport nous apprend que les scénarios d'inondation sont supposés représentatifs des périodes de retour 10, 100 et 1000 ans. J'ai trois nuances de vert...  Est-ce que si j'habite dans la zone en vert moyen j'ai une probabilité de 1/100 d'être inondé chaque année ??
**Pas tout à fait ! !** Quand on regarde séparément les 3 cartes des 3 scénarios, on comprend que chaque zone correspond à la zone inondée par la crue supposée représentative d'une période de retour T : c'est **à la limite de chaque zone** que j'ai une probabilité d'inondation de 1/T (une chance sur 100 pour une crue centennale...). Donc entre deux limites, dans une zone colorée avec l'une des nuances de vert, je sais juste que la probabilité d'inondation est **comprise entre celle pour la limite extérieure de la zone et celle pour la limite intérieure".**
Le petit schéma suivant essaie d'illustrer cela ; dans le wiki vous trouverez d'autres variantes.

<img src="/images/LegendeCarteInondation.png"  width="400">

Pour reboucler sur notre sac de billes, si j'assimile la période de retour de la bille (en débit) et celle de la carte ; si je tire une bille... :
- **rouge**, toutes les zones en vert foncé et vert clair sont inondées car c'est **au moins une crue centennale** ; l'inondation s'étend au-delà, mais l'information avec la bille n'est pas assez précise pour savoir ;
- **bleue** ou **verte** : la précision de la carte ne permet pas de distinguer les deux : le débit est entre la décennale et la centennale : l'inondation s'étend au-delà du vert moyen, mais en-deça du vert clair. Bien entendu, la limite sera plus loin pour une bille bleu ;
- **noire** : le débit est en-deça de sa valeur décennale, l'inondation est contenue dans l'étendue "vert foncé". 
